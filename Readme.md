# hypoor

Control your Linux Machine locally from your phone's browser

# Why?

Laziness, i keep forgetting to lock my PC or shut it down after going to bed or away,
and since my phone is almost always with me i can just click a button and save
myself the 10meter trip :)

Right now master `branch` is hardcoded commands but the unfinished `refactor`
branch allows for user defined commands.
