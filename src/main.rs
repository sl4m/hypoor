use std::net::SocketAddr;

use hyper::server::conn::Http;
use hyper::service::service_fn;
use hyper::{
    header::{HeaderName, HeaderValue},
    Body, Method, Request, Response, StatusCode,
};

use tokio::net::TcpListener;

use form_urlencoded;
use std::borrow::Borrow;
use std::borrow::Cow;
use tokio::process::Command;

type GenericError = Box<dyn std::error::Error + Send + Sync>;
type Result<T> = std::result::Result<T, GenericError>;

static INDEX: &str = "index.html";
static CSS: &str = "chota.min.css";

enum RemoteCmd {
    PowerOff,
    Reboot,
    Lock,
    ToggleHttpServer,
    GetStats,
    ShareText,
    Unknown,
    Ncspot(PlayerCmd),
    Pamixer(VolumeCmd),
}

enum VolumeCmd {
    Inc,
    Dec,
    ToggleMute,
}
enum PlayerCmd {
    PlayPause,
    Next,
    Prev,
}

impl From<Cow<'_, str>> for RemoteCmd {
    fn from(cmd_cow: Cow<'_, str>) -> RemoteCmd {
        match cmd_cow.borrow() {
            "lock" => RemoteCmd::Lock,
            "poweroff" => RemoteCmd::PowerOff,
            "reboot" => RemoteCmd::Reboot,
            "http" => RemoteCmd::ToggleHttpServer,
            "stats" => RemoteCmd::GetStats,
            "player.toggle" => RemoteCmd::Ncspot(PlayerCmd::PlayPause),
            "player.next" => RemoteCmd::Ncspot(PlayerCmd::Next),
            "player.prev" => RemoteCmd::Ncspot(PlayerCmd::Prev),
            "volume.tmute" => RemoteCmd::Pamixer(VolumeCmd::ToggleMute),
            "volume.inc" => RemoteCmd::Pamixer(VolumeCmd::Inc),
            "volume.dec" => RemoteCmd::Pamixer(VolumeCmd::Dec),
            "sharetxt" => RemoteCmd::ShareText,
            _ => RemoteCmd::Unknown,
        }
    }
}

async fn match_cmd(req: Request<Body>) -> Result<Response<Body>> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => {
            let ctx = Context::new();

            Ok(serve_file(INDEX, Some(ctx.await)).await.unwrap())
        }
        (&Method::GET, "/chota.min.css") => Ok(serve_file(CSS, None).await.unwrap()),
        (&Method::POST, "/exec") => {
            let whole_body = hyper::body::to_bytes(req).await?;

            let mut resp = Response::default();
            //*resp.status_mut() = StatusCode::RESET_CONTENT;
            *resp.status_mut() = StatusCode::SEE_OTHER;
            resp.headers_mut().insert(
                HeaderName::from_static("location"),
                HeaderValue::from_static("/"),
            );
            // get the cmd param
            match form_urlencoded::parse(whole_body.as_ref())
                .into_iter()
                .find(|(p, _)| p == "cmd")
            {
                Some((_, cmd)) => match cmd.into() {
                    RemoteCmd::PowerOff => {
                        // TODO: read cmds off a config file
                        exec(&["poweroff"]).await?;
                    }
                    RemoteCmd::Reboot => {
                        exec(&["reboot"]).await?;
                    }
                    RemoteCmd::Lock => {
                        exec(&["i3lock"]).await?;
                    }
                    RemoteCmd::ToggleHttpServer => {
                        exec(&["/usr/local/bin/http"]).await?;
                    }
                    RemoteCmd::GetStats => {}
                    RemoteCmd::Pamixer(vol_cmd) => match vol_cmd {
                        VolumeCmd::Inc => {
                            exec(&["pamixer", "-i", "5"]).await?;
                        }
                        VolumeCmd::Dec => {
                            exec(&["pamixer", "-d", "5"]).await?;
                        }
                        VolumeCmd::ToggleMute => {
                            exec(&["pamixer", "-t"]).await?;
                        }
                    },
                    RemoteCmd::Ncspot(spot_cmd) => match spot_cmd {
                        PlayerCmd::PlayPause => {
                            exec(&["playerctl", "play-pause"]).await?;
                        }
                        PlayerCmd::Prev => {
                            exec(&["playerctl", "previous"]).await?;
                        }
                        PlayerCmd::Next => {
                            exec(&["playerctl", "next"]).await?;
                        }
                    },

                    RemoteCmd::ShareText => {
                        match form_urlencoded::parse(whole_body.as_ref())
                            .into_iter()
                            .find(|(p, _)| p == "text")
                        {
                            Some((_, txt)) => {
                                use std::io::Write;
                                let file_pth = "/tmp/hypoor_sharedcnt.txt";
                                // is dangerous?
                                exec(&["notify-send", "New Shared Content"]).await?;
                                let mut file = std::fs::File::create(&file_pth)?;
                                file.write(&txt.as_bytes())?;
                                exec(&["xclip", "-rmlastnl", "-in", "-sel", "c", &file_pth])
                                    .await?;
                            }
                            None => {
                                *resp.status_mut() = StatusCode::UNPROCESSABLE_ENTITY;
                                *resp.body_mut() = "Missing Fields, try again\n".into();
                            }
                        }
                    }
                    RemoteCmd::Unknown => {
                        *resp.body_mut() = "unkown command, try again\n".into();
                    }
                },
                None => {
                    *resp.status_mut() = StatusCode::UNPROCESSABLE_ENTITY;
                    *resp.body_mut() = "Unprocessable Entity\n".into();
                }
            }
            Ok(resp)
        }
        _ => Ok(not_found()),
    }
}

async fn exec_out<'x>(cmd: &[&str]) -> std::result::Result<String, &'x str> {
    match Command::new(cmd[0])
        .args(&cmd[1..])
        .env("DISPLAY", ":0")
        .output()
        .await
    {
        Ok(output) => {
            if output.status.success() {
                Ok(String::from_utf8(output.stdout).unwrap())
            } else {
                Err("command ran uncorrectly")
            }
        }
        Err(_) => Err("command failed to run"),
    }
}
async fn exec<'x>(cmd: &[&str]) -> std::result::Result<(), &'x str> {
    match Command::new(cmd[0])
        .args(&cmd[1..])
        .env("DISPLAY", ":0")
        .stdout(std::process::Stdio::null())
        .stderr(std::process::Stdio::null())
        .status()
        .await
    {
        Ok(exit_code) => {
            if exit_code.success() {
                Ok(())
            } else {
                Err("command ran uncorrectly")
            }
        }
        Err(_) => Err("command failed to run"),
    }
}
struct Context<'c> {
    mute_stat: String,
    http_stat: &'c str,
}

impl<'c> Context<'c> {
    async fn new() -> Context<'c> {
        Self {
            mute_stat: exec_out(&["pamixer", "--get-mute"])
                .await
                .unwrap()
                .trim()
                .to_string(),
            http_stat: {
                if std::fs::File::open("/tmp/http.pid").is_ok() {
                    r#"<a href="javascript:;" onclick='window.location=`http://${window.location.hostname}`'/>running</a>"#
                } else {
                    "stopped"
                }
            },
        }
    }
}

async fn serve_file<'c>(filename: &str, ctx: Option<Context<'c>>) -> Result<Response<Body>> {
    let working_dir = std::env::args().nth(1).expect(&format!(
        "Provide a Working Dir with a {} and {} under it",
        INDEX, CSS
    ));

    let mut file_path = std::path::PathBuf::new();
    file_path.push(working_dir);
    file_path.push(filename);

    //println!("serving {:?}", file_path);
    if let Ok(contents) = tokio::fs::read(file_path).await {
        if let Some(ctx) = ctx {
            let resp = format!(
                include_str!("../www/index.html"),
                mute_stat = ctx.mute_stat,
                http_stat = ctx.http_stat
            );
            return Ok(Response::new(resp.into()));
        } else {
            return Ok(Response::new(contents.into()));
        }
    }
    Ok(not_found())
}

fn not_found() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("Not Found :(\n".into())
        .unwrap()
}

#[tokio::main]
async fn main() -> Result<()> {
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    let listener = TcpListener::bind(addr).await?;
    println!("Listening on http://{}", addr);

    loop {
        let (stream, client) = listener.accept().await?;
        println!("New Connection from {}", client);

        tokio::task::spawn(async move {
            if let Err(err) = Http::new()
                .serve_connection(stream, service_fn(match_cmd))
                .await
            {
                println!("Error serving connection: {:?}", err);
            }
        });
    }
}
